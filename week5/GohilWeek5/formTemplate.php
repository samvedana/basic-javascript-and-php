<!DOCTYPE html><html><head>
<title>Form</title>
    <meta charset="UTF-8">
<style>
  div { width: 20rem; border: solid 2px #acacac; padding: 2rem; }
</style>
</head>

<body>

<?php
// define variables and set to empty values
$firstName = $email = $subject = $message = "";

if ($_SERVER["REQUEST_METHOD"] == "POST") {
  $firstName = test_input($_POST["firstName"]);
  $email = test_input($_POST["Email"]);
  $subject = test_input($_POST["Subject"]);
  $message = test_input($_POST["Message"]);
}

function test_input($data) {
  $data = trim($data);
  $data = stripslashes($data);
  $data = htmlspecialchars($data);
  return $data;
}
?>


<div><h2 style = "text-align:center">Contact Me</h2>

<form name="contact" method="POST" action='<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>'>

<p>Your Name: <br>
<input type="text" name="firstName" ></p>
<p>Your E-mail: <br>
<input type="email" name="Email" required></p>
<p>Subject: <br>
<input type="text" name="Subject" /></p>
<p>Message:<br >
<textarea name="Message"></textarea></p>
<p><input type="reset" value="Clear Form" >&nbsp; &nbsp;
<input type="submit" name="Submit" value="Send Form" ></p>
</form>

</div>


<?php
echo "<h2>Your Input:</h2>";
echo $firstName;
echo "<br>";
echo $email;
echo "<br>";
echo $subject;
echo "<br>";
echo $message;
?>

<!--=====================
other option to do it:

<?php
if ($_SERVER["REQUEST_METHOD"] == "POST"){
if (isset ($_POST["firstname"]) && ($firstName!=null)){
$firstName = $_POST["firstName"];
}
echo $firstName;
}
?>
==========*/-->
</body>

</html>


