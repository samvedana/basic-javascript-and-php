<?php
$Pool = array('white', 'yellow','blue','red','violet','orange');
echo $Pool[1];
echo '<h1>print_r()</h1>';
print_r($Pool);
echo '<h1>var_dump()</h1>';
var_dump($Pool);
echo '<h1>var_export()</h1><br>';
var_export($Pool);

?>


<?php
$dog1 = array("breed"=>"Airedale","name"=>"Rusty","age"=>"6","color"=>"Black & Tan");
$dog2 = array();
$dog2["breed"] = "Welsh Terrier";
$dog2["name"] = "Sarge";
$dog2["age"] = "5";
$dog2["color"] = "Black & Tan";
echo '<br>';
echo $dog1["name"]. "is a ".$dog1["color"]. " ".$dog1["breed"];
echo '<br>';

echo $dog2["name"]. "is a ".$dog2["color"]. " ".$dog2["breed"];


?>