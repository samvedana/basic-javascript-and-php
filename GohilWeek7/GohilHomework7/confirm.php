<?php
$title = "newform.php";
  include 'header.php';
  ?>


<h1>Airedale - History of the Breed</h1>
<div class="intro"><p>Yes, I tend to work my dogs into the lecture now and then. I do quite a bit of online marketing and am constantly studying what gets attention. Dogs always seem to get an emotional response or at least a laugh.</p><p>This is Rusty, a large American Airedale Terrier born and bred to hunt. He is 27 inches tall.</p><p>The sturdy Airedale is the largest of all terriers. Males stand about 23 inches at the shoulder; females are a bit smaller. The wire coat is tan with dark markings. Rangy but muscular legs give Airedales a regal lift in their bearing, and the long head—with its sporty beard and mustache, expressive eyes, and neatly folded ears—conveys a keen intelligence. Airedales are the picture of an alert and willing terrier—and then some. <a href="http://www.akc.org/dog-breeds/airedale-terrier/detail/" target="_blank">Source</a></p><p>Airedale, a valley (dale) in the West Riding of Yorkshire, between the Aire and the Wharfe Rivers, was the birthplace of the breed. In the mid-19th Century, working class people created the Airedale Terrier by crossing the old English rough-coated Black and Tan Terrier with the Otterhound. </p><p>In 1886, the Kennel Club of England formally recognised the Airedale Terrier breed. In 1864 they were exhibited for the first time at a championship dog show sponsored by the Airedale Agricultural Society. They were classified under different names, including Rough Coated, Bingley and Waterside Terrier. In 1879 breed fanciers decided to call the breed the Airedale Terrier, a name accepted by the Kennel Club (England) in 1886. <a href="http://www.saredon.org/our_breeds/airedales/" target="_blank">Source</a></p></div>
</div>


<?php
  include 'footer.php';
  ?>
</body>
</html>


