<?php
$title = "newform.php";
  include 'header.php';
  ?>

<?php
// define variables and set to empty values
$firstName = $subject = $email = $phone = $message  =  $nameErr = $subjectErr = $emailErr = $phoneErr = "";

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    
  if (empty($_POST["firstName"])) {
    $nameErr = "Name is required";
  } else {
  $firstName = test_input($_POST["firstName"]);
    // check if name only contains letters and whitespace
    if (!preg_match("/^[a-zA-Z ]*$/",$firstName)) {
      $nameErr = "Only letters and white space allowed"; 
    }
  }
  
  
   if (empty($_POST["email"])) {
    $emailErr = "Email is required";
  } else {
   $email = test_input($_POST["email"]);
    // check if e-mail address is well-formed
    if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
      $emailErr = "Invalid email format"; 
    }
  }
  

  $subject = test_input($_POST["subject"]);
  $phone = test_input($_POST["phone"]);
  $message = test_input($_POST["message"]);

}


function test_input($data) {
  $data = trim($data);
  $data = stripslashes($data);
  $data = htmlspecialchars($data);
  return $data;
}
?>



<div class="intro" >
<h2>Tell me about yourself</h2>
<form name="contact" id="survey" method="POST" action='<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>'>

<p><span class="error">* required field</span></p>


<p>Your Name: 
<input type="text" name="firstName" id="firstName" value="<?php echo $firstName;  ?>" >
<span class="error">*</span>
  <span class="error" id="error"> <?php if(isset($nameErr)) {echo $nameErr;}?></span>

</p>
<p>Subject: 
<input type="text" name="subject" id="subject"  value="<?php  echo $subject;  ?>">

</p>
<br>

<fieldset><legend>Contact Info</legend>
<p>Your E-mail: 
<input type="text" name="email" id="email" value="<?php  echo $email;  ?>" >
<span class="error">*</span>

  <span class="error" id="error"> <?php  if(isset($nameErr)) {echo $emailErr;}?></span>

</p>
<p>Your Phone number: 
<input type="text" name="phone" id="phone"  value="<?php  echo $phone;  ?>">

</p>
</fieldset>


<br>

<fieldset><legend>Add relevant classes</legend>
<ul id="extra">
</ul>
<button type="button" class="add" onclick="buildExtraField()">Add Class</button>
</fieldset>


<p>Message:
<textarea name="message" id="message"><?php echo $message;  ?></textarea></p>
<input type="checkbox" name="agree" id="agree" value="ok" onclick="iAgree()">Agree<br>

<p><input type="button" name="clear" onclick="clearForm()" value="Clear Form">&nbsp; &nbsp;
<input type="button" name="Submit" value="Send Form" id="submit" ></p>
<br>
</form>

</div>


<?php
echo "<h2>Your Input:</h2>";
echo $firstName;
echo "<br>";
echo $subject;
echo "<br>";
echo $email;
echo "<br>";
echo $phone;
echo "<br>";
echo $message;
echo "<br>";
echo "Thanks "."$firstName"."! We'll get back to you soon.";
?>


<?php
  include 'footer.php';
  ?>


<style>
.error {color: #FF0000;}
</style>

</body>
</html>


