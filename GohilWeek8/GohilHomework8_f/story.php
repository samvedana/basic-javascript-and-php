<?php
$title = "newform.php";
  include 'header.php';
  ?>


<h1>History of the San Francisco</h1>
<div class="intro"><p>
The history of the city of San Francisco, California, and its development as a center of maritime trade, were shaped by its location at the entrance to a large natural harbor. San Francisco is the name of both the city and the county; the two share the same boundaries. Starting overnight as the base for the gold rush of 1849, the city quickly became the largest and most important population, commercial, naval, and financial center in the American West. 
<p>San Francisco was devastated by a great earthquake and fire in 1906 but was quickly rebuilt. The San Francisco Federal Reserve Branch opened in 1914, and the city continued to develop as a major business city throughout the first half of the 20th century. Starting in the latter half of the 1960s, San Francisco became the city most famous for the hippie movement. In recent decades, San Francisco has become an important center of finance and technology. The high demand for housing, driven by its proximity to Silicon Valley, and the low supply of available housing has led to the city being one of America's most expensive places to live. San Francisco is currently ranked ninth on the Global Financial Centres Index.</p> 
</p>

</div>
</div>


<?php
  include 'footer.php';
  ?>
</body>
</html>


