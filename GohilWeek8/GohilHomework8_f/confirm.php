<?php
global $firstName, $subject, $email, $phone, $message, $nameErr, $subjectErr, $emailErr, $phoneErr, $alert, $errCount;

function validateForm() {
$errCount = false;
// define variables and set to empty values


if ($_SERVER["REQUEST_METHOD"] == "POST") {

  if (empty($_POST["firstName"])) { 
    $nameErr = "Name is required";
    $errCount=true;
     } 
  else {
    $firstName = test_input($_POST["firstName"]);
    // check if name only contains letters and whitespace
    if (!preg_match("/^[a-zA-Z ]*$/",$firstName)) { 
    $nameErr = "Only letters and white space allowed"; 
    $errCount=true;
     }
    }  
     
     
   if (empty($_POST["email"])) { 
    $emailErr = "Email is required";
    $errCount=true;
    } else {
     $email = test_input($_POST["email"]);
    // check if e-mail address is well-formed
     if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
         $emailErr ="Invalid email format";
         $errCount=true;
        }
    }  
  
  if (!empty($_POST["subject"])) {
  $subject = test_input($_POST["subject"]); 
  }
  if (!empty($_POST["phone"])) {
  $phone = test_input($_POST["phone"]);
  } 
  if (!empty($_POST["message"])) {
  $message = test_input($_POST["message"]);
  }
 }
 
 
 if($errCount==false)
 {
   echo "<h2>Your Input:</h2>"; echo $firstName;
    echo "<br>"; echo $subject; echo "<br>"; echo $email; echo "<br>"; echo
    $phone; echo "<br>"; echo $message; echo "<br>"; echo "Thanks
    "."$firstName"."! We'll get back to you soon.";
    
 } else {
 return '<span class="error">Please correct errors in the form.</span>';
 }
}


    function test_input($data) {
     $data = trim($data); 
     $data =stripslashes($data); 
     $data = htmlspecialchars($data); 
     return $data;
    }


?>