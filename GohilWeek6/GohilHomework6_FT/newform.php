<?php
$title = "newform.php";
  include 'header.php';
  ?>

<?php
// define variables and set to empty values
$firstName = $subject = $email = $phone = $message  = "";

if ($_SERVER["REQUEST_METHOD"] == "POST") {
  $firstName = test_input($_POST["firstName"]);
  $email = test_input($_POST["email"]);
  $subject = test_input($_POST["subject"]);
  $phone = test_input($_POST["phone"]);
  $message = test_input($_POST["message"]);

}

function test_input($data) {
  $data = trim($data);
  $data = stripslashes($data);
  $data = htmlspecialchars($data);
  return $data;
}
?>

<div class="intro" >
<h2>Tell me about yourself</h2>
<form name="contact" id="survey" method="POST" action='<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>'>

<p>Your Name: 
<input type="text" name="firstName" id="firstName" value="<?php  echo $firstName;  ?>" ></p>
<p>Subject: 
<input type="text" name="subject" id="subject"  value="<?php echo $subject;  ?>"></p>
<br>

<fieldset><legend>Contact Info</legend>
<p>Your E-mail: 
<input type="text" name="email" id="email" value="<?php echo $email;  ?>" ></p>
<p>Your Phone number: 
<input type="text" name="phone" id="phone"  value="<?php echo $phone;  ?>"></p>
</fieldset>


<br>

<fieldset><legend>Add relevant classes</legend>
<ul id="extra">
</ul>
<button type="button" class="add" onclick="buildExtraField()">Add Class</button>
</fieldset>


<p>Message:
<textarea name="message" id="message"></textarea></p>
<input type="checkbox" name="agree" id="agree" value="ok" onclick="iAgree()">Agree<br>

<p><input type="button" name="clear" onclick="clearForm()" value="Clear Form">&nbsp; &nbsp;
<input type="submit" name="Submit" value="Send Form" id="submit" ></p>
</form>

</div>


<?php
echo "<h2>Your Input:</h2>";
echo $firstName;
echo "<br>";
echo $subject;
echo "<br>";
echo $email;
echo "<br>";
echo $phone;
echo "<br>";
echo $message;
?>


<?php
  include 'footer.php';
  ?>




</body>
</html>


